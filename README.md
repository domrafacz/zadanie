# Komentarz

Wszystkie operacje z 8 punktów są możliwe do wykonania w tym api.
W kwesti punktu 4 dodałem opcje filtrowania po nazwie, ale nie da się przeprowadzić walidacji parametru na operacji GetCollection() w api-platform (w łatwy sposób).
Nie wystawiałem osobnych endpointów do każdej operacji ze względu na ograniczony czas i moją ograniczoną wiedzę co do api-platform. Przeprowadziłem testy manualne przy pomocy postmana. Posiadając więcej czasu napisałbym testy za pomocą PHPUnit (ApiTestCase).

# Dokumentacja

```bash
http://localhost/api
```

# Instalacja

```bash
composer install --dev
```